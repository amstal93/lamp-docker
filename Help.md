### Solucion a problemas

** Problema 

* Para conocer las IPs de los contenedores ejecutar: 

sudo docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)

* Error:

Got permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get "http://%2Fvar%2Frun%2Fdocker.sock/v1.24/containers/json": dial unix /var/run/docker.sock: connect: permission denied

* Solución:

sudo chmod 666 /var/run/docker.sock
